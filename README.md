# PPK-Praktikum 9 : Android Life Cycle 

## Identitas

```
Nama : Novanni Indi Pradana
NIM  : 222011436
Kelas: 3SI3

```

## Deskripsi

Sebagaimana dijelaskan pada halaman https://developer.android.com/guide/components/activities/intro-activities?hl=id bahwa Class Activity adalah komponen penting aplikasi Android, dan cara aktivitas diluncurkan dan disatukan adalah bagian mendasar dari model aplikasi platform. Tidak seperti paradigma pemrograman di mana aplikasi diluncurkan dengan metode main(), sistem Android memulai kode dalam instance Activity dengan memanggil metode callback tertentu yang sesuai dengan tahapan tertentu dari siklus prosesnya.


## Kegiatan Praktikum

### Activity on Virtual Device
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-9/-/raw/main/screenshot/Screenshot.png)
### Activity on Personal Device
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-9/-/raw/main/screenshot/redmi_note_5_hello_world.jpeg)
![cd_catalog.xml](https://gitlab.com/PradanaIN/ppk-praktikum-9/-/raw/main/screenshot/Screenshot2.png)


## Android Activity

### onCreate()
Dilakukan saat inisiasi aktivitas atau kondisi ketika menjalankan aktivitas pertama kali, sehingga akan terbentuk status lifecycle onCreate().
### onStart()
onStart() akan dipanggil secara otomatis ketika onCreate() telah dipanggil, terjadi ketika aplikasi dibuka oleh pengguna.
### onResume()
onResume() akan terbentuk ketika aktivitas mulai berinteraksi dengan pengguna yang merupakan indikator penanda bahwa aktivitas menjadi aktif dan siap menerima masukan. Keadaan ini terkadang juga bisa menjadi keadaan transit menuju keadaan istirahat lainnya. 
### onPause()
onPause() akan terjadi ketika bagian dari siklus aktivitas pengguna tidak lagi berinteraksi secara aktif, akan tetapi masih terlihat di layar. Kondisi ini merupakan lanjutan dari onResume(). Kondisi ini terjadi pada saat aktivitas B diluncurkan di depan aktivitas A, callback ini akan dipanggil pada aktivitas A. Kemudian aktivitas B tidak akan dibuat sehingga onPause() aktivitas A Kembali.
### onStop()
onStop() dipanggil ketika tampilan tidak lagi terlihat oleh pengguna. Selanjutnya akan menerima onRestart(), onDestroy(), atau tidak sama sekali, bergantung pada aktivitas pengguna selanjutnya. Method ini merupakan aktivitas atau tempat yang baik untuk berhenti menyegarkan UI, menjalankan animasi, dan hal-hal visual lainnya.
### onDestroy()
onDestroy() ini akan dilakukan dengan pembersihan akhir sebelum aktivitas dimusnahkan. Hal ini dapat terjadi baik karena aktivitas sedang selesai (seseorang memanggil finish() di atasnya), atau karena sistem sementara menghancurkan instance aktivitas ini untuk menghemat ruang.
### onRestart()
Dipanggil setelah onStop() saat aktivitas saat ini ditampilkan kembali kepada pengguna (pengguna telah menavigasi kembali ke sana). Ini akan diikuti oleh onStart() dan kemudian onResume().
